upstream docker-registry {
	server {{ registry }};
}

server {
        listen 80;
        listen [::]:80;

        server_name {{ registry_domain }};

	# required to avoid HTTP 411: see Issue #1486 (https://github.com/moby/moby/issues/1486)
	chunked_transfer_encoding on;

        location /v2/ {
		proxy_set_header  Host              $http_host;   # required for docker client's sake
		proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
		proxy_set_header  X-Forwarded-Proto $scheme;
		proxy_read_timeout 900;

		if ($request_method ~* "^(GET|HEAD)$") {
			proxy_pass                          http://docker-registry;
		}
        }
}
